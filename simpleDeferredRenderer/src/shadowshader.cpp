//
// Created by pbartels on 8/9/20.
//

#include "shadowshader.h"

namespace glAislinn
{
    ShadowShader::ShadowShader(const std::string &vertexShaderSourceFile,
                               const std::string &fragmentShaderSourceFile)
                               : Shader(vertexShaderSourceFile, fragmentShaderSourceFile)
    {
        setCameraViewBindPoint(shadowViewSettingsUniformBlockBindPoint);
    }
}