//
// Created by pieterjan on 24/07/2020.
//

#include <filesystem>
#include <fstream>

#include "shader.h"

namespace glAislinn
{

    ShaderUniform::ShaderUniform(std::string name, ValueType value) : m_name(name), m_value(value) {}

    const std::string& ShaderUniform::name() const
    {
        return m_name;
    }

    ShaderUniform::ValueType ShaderUniform::value() const
    {
        return m_value;
    }

    Shader::Shader(const std::string & vertexShaderSourceFile,
                   const std::string & fragmentShaderSourceFile) : m_program(), m_hasViewSupport(false)
    {

        // First, create both shaders on the GPU, read in and compile the source
        GLuint vertexShader = compileShader(vertexShaderSourceFile, GL_VERTEX_SHADER);
        GLuint fragmentShader = compileShader(fragmentShaderSourceFile, GL_FRAGMENT_SHADER);

        // create a shader program, and attach the two compiled shaders
        m_program = glCreateProgram();
        glAttachShader(m_program, vertexShader);
        glAttachShader(m_program, fragmentShader);
        // link the different shaders attached to the program together
        glLinkProgram(m_program);

        // Check if linking succeeded (similar to compilation process below. If not, throw error with explanatory message.
        GLint linkFlag;
        glGetProgramiv(m_program, GL_LINK_STATUS, &linkFlag);
        if(!linkFlag)
        {
            char infoLog[512];
            glGetProgramInfoLog(m_program, 512, nullptr, infoLog);
            std::string errorMessage(infoLog);
            throw std::runtime_error("Program linking error: " + errorMessage);
        }

        // delete the no longer needed shaders (i.e. once they are compiled, the source is no longer needed.
        //      In other words, the shader objects represent the source).
        glDeleteShader(vertexShader);
        glDeleteShader(fragmentShader);

        setCameraViewBindPoint(viewSettingsUniformBlockBindPoint);
        setViewBindPoint("ShadowView", shadowViewSettingsUniformBlockBindPoint);
    }

    Shader::Shader(Shader&& other) noexcept
    {
        m_program = other.m_program;
        m_hasViewSupport = other.m_hasViewSupport;

        // make sure the other one doesn't delete our shader program when it goes out of scope.
        other.m_program = 0;
    }

    void Shader::use() const
    {
        if(m_program == 0) throw std::invalid_argument("Trying to use an invalid shader object. Have you moved this instance?");
        glUseProgram(m_program);
    }

    Shader::~Shader()
    {
        glDeleteProgram(m_program);
    }

    bool Shader::setUniform(const std::string & uniformName, float value) const
    {
        GLint uniLocation = uniformLocation(uniformName);
        if(uniLocation < 0) return false;

        use();
        glUniform1f(uniLocation, value);
        return true;
    }

    bool Shader::setUniform(const std::string & uniformName, int value) const
    {
        GLint uniLocation = uniformLocation(uniformName);
        if(uniLocation < 0) return false;

        use();
        glUniform1i(uniLocation, value);
        return true;
    }

    bool Shader::setUniform(const std::string & uniformName, Vec3f value) const
    {
        GLint uniLocation = uniformLocation(uniformName);
        if(uniLocation < 0) return false;

        use();
        glUniform3f(uniLocation, value.x(), value.y(), value.z());
        return true;
    }

    bool Shader::setUniform(const std::string & uniformName, Vec4f value) const
    {
        GLint uniLocation = uniformLocation(uniformName);
        if(uniLocation < 0) return false;

        use();
        glUniform4f(uniLocation, value.x(), value.y(), value.z(), value.w());
        return true;
    }

    bool Shader::setUniform(const std::string & uniformName, Mat3f value) const
    {
        GLint uniLocation = uniformLocation(uniformName);
        if(uniLocation < 0) return false;

        use();
        glUniformMatrix3fv(uniLocation, 1, GL_FALSE, value.data());
        return true;
    }

    bool Shader::setUniform(const std::string & uniformName, Mat4f value) const
    {
        GLint uniLocation = uniformLocation(uniformName);
        if(uniLocation < 0) return false;

        use();
        glUniformMatrix4fv(uniLocation, 1, GL_FALSE, value.data());
        return true;
    }

    bool Shader::setUniform(ShaderUniform uniform) const
    {
        ShaderUniform::ValueType value = uniform.value();
        return std::visit([&](auto&& v)->bool{return this->setUniform(uniform.name(), v);}, value);
    }


    bool Shader::hasViewSupport() const
    {
        return m_hasViewSupport;
    }

    std::string Shader::readFile(const std::string & filename) const
    {
        // check if the path exists before using it.
        //  Using std::filesystem from C++17
        if(!std::filesystem::exists(std::filesystem::path(filename)))
            throw std::invalid_argument("File given for shader source does not exist.");

        std::ifstream inputStream(filename);
        std::stringstream buffer;
        buffer << inputStream.rdbuf();
        return buffer.str();
    }

    GLuint Shader::compileShader(const std::string & shaderSourceFilename, GLenum shaderType) const
    {
        // create a shader program of the requested type on the GPU
        GLuint shader = glCreateShader(shaderType);

        // Read in the file, convert the shader source to a pointer to pointer.
        std::string source = readFile(shaderSourceFilename).c_str();
        const char * source_c_str = source.c_str();
        // send the shader source to the gpu shader object
        glShaderSource(shader, 1, &source_c_str, nullptr);

        // compile the shader object
        glCompileShader(shader);

        // check if the compilation succeeded: check the error code, if it's non-zero, read the message to a buffer
        GLint compilationFlag;
        // this gets the requested parameter (compile status) and puts it in compilationFlag.
        glGetShaderiv(shader, GL_COMPILE_STATUS, &compilationFlag);
        if(!compilationFlag)
        {
            // if it failed, get the message and throw an error:
            char infoLog[512];
            // same deal as above: query the shader data structure
            glGetShaderInfoLog(shader, 512, nullptr, infoLog);
            std::string errorMessage(infoLog);
            throw std::runtime_error("Shader compilation error: " + errorMessage);
        }

        // return the compiled shader
        return shader;
    }

    GLint Shader::uniformLocation(const std::string & uniformName) const
    {
        return glGetUniformLocation(m_program, uniformName.c_str());
    }

    void Shader::setCameraViewBindPoint(GLuint bindPointNumber)
    {
        m_hasViewSupport = setViewBindPoint("View", bindPointNumber);
    }

    bool Shader::setViewBindPoint(const std::string& uniformBlockName, GLuint bindPointNumber)
    {
        int blockIndex = glGetUniformBlockIndex(m_program, uniformBlockName.c_str());

        if(blockIndex != GL_INVALID_INDEX)
        {
            glUniformBlockBinding(m_program, blockIndex, bindPointNumber);
            return true;
        }
        return false;
    }

}