//
// Created by pieterjan on 04/08/2020.
//

#include "light.h"

namespace glAislinn
{

    Light::Light(const Shader& shader,
                 const ShadowShader& shadowShader,
                 Vec3f position,
                 Vec4f intensity,
                 Vec4f viewProperties,
                 Vec3f target, Vec3f up) :
                 m_shader(shader), m_shadowShader(shadowShader),
                 m_view(viewProperties.x(),
                        viewProperties.y(),
                        viewProperties.z(),
                        viewProperties.w(), position),
                 m_intensity(intensity),
                 m_shadowFrameBuffer(0),
                 m_shadowDepthMap(0)
    {
        setOrientation(target, up);

        // set up shadow map buffer
        glGenFramebuffers(1, &m_shadowFrameBuffer);
        // bind it so we can set its properties
        glBindFramebuffer(GL_FRAMEBUFFER, m_shadowFrameBuffer);

        // create the depth map
        glGenTextures(1, &m_shadowDepthMap);
        // bind it so we can set its properties
        glBindTexture(GL_TEXTURE_2D, m_shadowDepthMap);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, shadowMapResolution, shadowMapResolution, 0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        // we set the border color to white and make sure the shadow are not repeated outside of the view (happens when texture is accessed with out of range uvs)
        std::array<float, 4> borderColor = {1.f, 1.f, 1.f, 1.f};
        glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor.data());
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

        // attach the depth map to the framebuffer
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, m_shadowDepthMap, 0);

        // Explicitly tell opengl we do *not* want a color buffer for this depth map
        glDrawBuffer(GL_NONE);
        glReadBuffer(GL_NONE);

        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
            throw std::runtime_error("Something went wrong while creating the Shadow framebuffer");
    }

    // Movable. Defaults no longer okay.
    Light::Light(Light&& other) noexcept
                                : m_shader(other.m_shader),
                                  m_shadowShader(other.m_shadowShader),
                                  m_view(std::move(other.m_view)),
                                  m_intensity(other.m_intensity),
                                  m_shadowFrameBuffer(other.m_shadowFrameBuffer),
                                  m_shadowDepthMap(other.m_shadowDepthMap)
    {
        // view has been moved already, so that's fine

        // make sure the other two GPU resources are pillaged.
        other.m_shadowFrameBuffer = 0;
        other.m_shadowDepthMap = 0;
    }

    Light& Light::operator=(Light&& other) noexcept
    {
        if(this != std::addressof(other))
        {
            // free our own resources
            glDeleteFramebuffers(1, &m_shadowFrameBuffer);
            glDeleteTextures(1, &m_shadowDepthMap);
            // pillage the ones from the other
            m_shadowFrameBuffer = other.m_shadowFrameBuffer;
            m_shadowDepthMap = other.m_shadowDepthMap;
            other.m_shadowFrameBuffer = 0;
            other.m_shadowDepthMap = 0;

            // pillage the view
            m_view = std::move(other.m_view);

            // copy trivial members
            m_shader = other.m_shader; // not exactly trivial, but explicitly not owned by this class
            m_shadowShader = other.m_shadowShader; // same as above
            m_intensity = other.m_intensity;

        }
        return *this;
    }

    // default no longer okay
    Light::~Light()
    {
        // it will call delete on the view by itself, so that's fine.
        // https://en.cppreference.com/w/cpp/language/destructor
        // We only have to clean up the other two GPU resources

        glDeleteFramebuffers(1, &m_shadowFrameBuffer);
        glDeleteTextures(1, &m_shadowDepthMap);
    }


    void Light::setPosition(Vec3f position)
    {
        m_view.setPosition(position);
    }

    void Light::move(Vec3f delta)
    {
        m_view.movePosition(delta);
    }

    Vec3f Light::position() const
    {
        return m_view.position();
    }

    void Light::setOrientation(Vec3f target, Vec3f up)
    {
        m_view.lookAt(target, up);
    }

    void Light::setIntensity(Vec4f intensity)
    {
        m_intensity = intensity;
    }

    Vec4f Light::intensity() const
    {
        return m_intensity;
    }


    const Shader& Light::shader() const
    {
        return m_shader;
    }

    void Light::use(GLenum shadowTextureUnit) const
    {
        const Shader& shader = m_shader.get();
        shader.use();
        shader.setUniform("lightPosition", position());
        shader.setUniform("lightIntensity", intensity());

        glActiveTexture(shadowTextureUnit);
        glBindTexture(GL_TEXTURE_2D, m_shadowDepthMap);
        shader.setUniform("shadow", (int) (shadowTextureUnit - GL_TEXTURE0));

        syncView();
    }

    void Light::drawShadowMap(Span<const Drawable> drawables) const
    {
        // set up drawing the shadow map buffer
        glViewport(0, 0, shadowMapResolution, shadowMapResolution);
        glBindFramebuffer(GL_FRAMEBUFFER, m_shadowFrameBuffer);
        glEnable(GL_DEPTH_TEST);
        glClear(GL_DEPTH_BUFFER_BIT);
        glDisable(GL_BLEND);
        // enable front face culling to combat peter panning and shadow rash
        glEnable(GL_CULL_FACE);
        glCullFace(GL_FRONT);

        // set view uniform bind point to the lights shadow view
        syncView();

        for(const Drawable & drawable: drawables)
        {
            // we can draw drawables without material properties
            drawable.draw(m_shadowShader);
        }

        // disable these, as the shadow map is the only one using this
        glCullFace(GL_BACK);
        glDisable(GL_CULL_FACE);
    }

    void Light::syncView() const
    {
        m_view.sync(shadowViewSettingsUniformBlockBindPoint);
    }
}