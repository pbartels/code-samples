//
// Created by pieterjan on 25/07/2020.
//

#include "drawable.h"

namespace glAislinn
{
    Drawable::Drawable(const IndexedMesh& mesh, const MaterialProperties& materialProperties, Affine3f transform) :
                                                    m_modelTransform(), m_normalTransform(),
                                                    m_vertexData(mesh), m_materialProperties(materialProperties)
    {
        setTransform(transform);
    }

    void Drawable::setTransform(Affine3f transform)
    {
        m_modelTransform = transform;
        // compute the inverse (for normals) as few times as possible, and on the CPU side.
        m_normalTransform = transform.linear().inverse().transpose();
    }

    void Drawable::applyTransform(Affine3f transform)
    {
        Affine3f newTransform = transform * m_modelTransform;
        setTransform(newTransform);
    }

    void Drawable::preTransform(Affine3f transform)
    {
        Affine3f newTransform = m_modelTransform * transform;
        setTransform(newTransform);
    }

    void Drawable::clearTransform()
    {
        setTransform(Affine3f());
    }


    void Drawable::draw(const Shader & shader, Span<const GLuint> materialTextureUnits) const
    {
        // we might want to re-draw the geometry with several shaders
        // Make sure the shader is active
        shader.use();

        // send material properties and model transform to shader
        shader.setUniform("model.transform", m_modelTransform.matrix());
        shader.setUniform("model.normalTransform", m_normalTransform);

        for(const ShaderUniform& uniform: m_materialProperties.get().uniforms()) shader.setUniform(uniform);
        m_materialProperties.get().bindTextures(materialTextureUnits);

        // draw mesh
        m_vertexData.get().draw();
    }

    void Drawable::draw(const Shader& shader) const
    {
        shader.use();

        // send material properties and model transform to shader
        shader.setUniform("model.transform", m_modelTransform.matrix());
        shader.setUniform("model.normalTransform", m_normalTransform);

        m_vertexData.get().draw();
    }

}