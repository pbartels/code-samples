//
// Created by pieterjan on 28/07/2020.
//

#include "geometrybuffer.h"

namespace glAislinn
{

    GeometryBuffer::GeometryBuffer(unsigned int width, unsigned int height)
                        : c_width(width), c_height(height),
                          m_framebuffer(0), m_textures(),
                          m_gBufferShader("../glAislinn/shaders/geometry.vert",
                                          "../glAislinn/shaders/geometry.frag")
    {
        // Create a framebuffer object (FBO) on the GPU side.
        glGenFramebuffers(1, &m_framebuffer);
        // Bind it so we can start setting it up.
        // GL_FRAMEBUFFER is one of three options (the other being either read or write, this one being both).
        glBindFramebuffer(GL_FRAMEBUFFER, m_framebuffer);

        // Set up several buffers. This means we will be using Multiple Render Targets
        glGenTextures(m_textures.size(), m_textures.data());

        // Position, Normal, Albedo, Material Properties
        for(size_t buffer = 0; buffer < m_textures.size() - 1; ++buffer)
        {
            // Create a texture for position (m_textures[0]), normal (m_textures[1]),
            //                      albedo(m_textures[2]) and material properties (m_textures[3])
            // bind the first texture in the array to GL_TEXTURE_2D so we can set it up.
            glBindTexture(GL_TEXTURE_2D, m_textures[buffer]);
            // specify the texture by setting up stuff for the target we just bound to
            // https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glTexImage2D.xhtml
            // we pass a nullptr because we have no data for this one. We are going to render to it.
            // Notice that we use floats for position and normal, which allow negative values
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, c_width, c_height, 0, GL_RGBA, GL_FLOAT, nullptr);
            // Set the magnification and minification filter
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

            // Attach the texture to the buffer
            // This is what is actually stored in the framebuffer object! The texture creation is separate, in a way.
            // We attach the m_textures[0] to the color attachments defined in the header file.
            // Last one is the level we are assigning to the framebuffer.
            glFramebufferTexture2D(GL_FRAMEBUFFER, c_renderTargets[buffer], GL_TEXTURE_2D, m_textures[buffer], 0 /*level, not a pointer*/);
        }


        // Create a texture for depth: similar to above but different format
        glBindTexture(GL_TEXTURE_2D, m_textures[m_textures.size() - 1]);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, c_width, c_height, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, nullptr);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, m_textures[m_textures.size() - 1], 0);

        // Set up MULTIPLE RENDER TARGETS
        // This allows the geometryBuffer fragment shader to declare multiple outputs.
        glDrawBuffers(c_renderTargets.size(), c_renderTargets.data());

        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
            throw std::runtime_error("Something went wrong while creating the Geometry Buffer");
    }

    GeometryBuffer::~GeometryBuffer()
    {
        glDeleteFramebuffers(1, &m_framebuffer);
        glDeleteTextures(m_textures.size(), m_textures.data());
    }


    void GeometryBuffer::draw(Span<const Drawable> drawables) const
    {
        // We are going to render into the geometrybuffer, not the default buffer.
        glViewport(0, 0, c_width, c_height);
        glBindFramebuffer(GL_FRAMEBUFFER, m_framebuffer);
        glEnable(GL_DEPTH_TEST); // we need depth testing, obviously
        glDisable(GL_BLEND); // We don't want blending! - this will be turned on in the lighting pass so we explicitly disable it.
        glClearColor(0.f, 0.f, 0.f, 1.0f); // background is black -- expose this later
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear the buffers

        m_gBufferShader.setUniform("albedo", c_materialTextureUniforms[0]);
        m_gBufferShader.setUniform("roughness", c_materialTextureUniforms[1]);
        m_gBufferShader.setUniform("metalness", c_materialTextureUniforms[2]);

        for(const Drawable& drawable: drawables)
        {
            // we need to read in material properties here first.

            drawable.draw(m_gBufferShader, c_materialTextureUnits);
        }
    }


    void GeometryBuffer::bindTextures(Span<const GLenum> textureUnits) const
    {
        for(unsigned int buffer = 0; buffer < textureUnits.size(); ++buffer)
        {
            // bind the textures we rendered to the specific texture units
            glActiveTexture(textureUnits[buffer]);
            glBindTexture(GL_TEXTURE_2D, m_textures[buffer]);
        }
    }

}