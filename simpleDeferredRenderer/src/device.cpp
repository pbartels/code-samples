//
// Created by pieterjan on 23/07/2020.
//

#include <iostream>

#include "device.h"

namespace glAislinn
{

    Backend::Backend(int screenWidth, int screenHeight) : m_window(nullptr)
    {
        // initialize glfw
        glfwInit();
        // set windowing options
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

        glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

        // create glfw window and check if succeeded
        m_window = glfwCreateWindow(screenWidth, screenHeight, "Aislinn", nullptr, nullptr);
        if(m_window == nullptr)
        {
            glfwTerminate();
            throw std::runtime_error("Failed to create GLFW window");
        }

        // make the OpenGL context created by glfw the main context on this thread.
        // from here on out all gl calls go to that context
        glfwMakeContextCurrent(m_window);

        if(!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress))
        {
            throw std::runtime_error("Failed to initialize GLAD");
        }

        double posX, posY;
        glfwGetCursorPos(m_window, &posX, &posY);
        m_userControlValues = {posX, posY, 0, 0, Vec2f{0, 0}};

    }

    Backend::~Backend()
    {
        // release resources taken by glfw
        glfwTerminate();
    }

    bool Backend::keepRendering () const
    {
        return !glfwWindowShouldClose(m_window);
    }

    UserControl Backend::startRenderLoop()
    {
        // process glfw inputs
        processInputs();
        return m_userControlValues;
    }

    void Backend::finishRenderLoop() const
    {
        GLenum error = glGetError();
        if(error > 0) std::cout << "OpenGL is in an error state. Error code:" << error << std::endl;

        // swap drawing and display buffer
        glfwSwapBuffers(m_window);
        // get input events
        glfwPollEvents();
    }

    double Backend::getTimeInSeconds() const
    {
        return glfwGetTime();
    }


    UserControl Backend::userControl() const
    {
        return m_userControlValues;
    }


    void Backend::processInputs()
    {
        Vec2f newArrowKeys(0, 0);

        if(glfwGetKey(m_window, GLFW_KEY_ESCAPE) == GLFW_PRESS) glfwSetWindowShouldClose(m_window, true);


        if(glfwGetKey(m_window, GLFW_KEY_UP) == GLFW_PRESS) newArrowKeys += Vec2f(0, 1);
        if(glfwGetKey(m_window, GLFW_KEY_DOWN) == GLFW_PRESS) newArrowKeys += Vec2f(0, -1);
        if(glfwGetKey(m_window, GLFW_KEY_RIGHT) == GLFW_PRESS) newArrowKeys += Vec2f(0, 1);
        if(glfwGetKey(m_window, GLFW_KEY_LEFT) == GLFW_PRESS) newArrowKeys += Vec2f(0, -1);


        double newPosX, newPosY;
        glfwGetCursorPos(m_window, &newPosX, &newPosY);
        m_userControlValues.mouseDeltaX = m_userControlValues.mousePositionX - newPosX;
        m_userControlValues.mouseDeltaY = m_userControlValues.mousePositionY - newPosY;
        m_userControlValues.mousePositionX = newPosX;
        m_userControlValues.mousePositionY = newPosY;
        m_userControlValues.arrows = newArrowKeys;
    }

}