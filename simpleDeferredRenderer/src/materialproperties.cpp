//
// Created by pieterjan on 30/07/2020.
//

#include "materialproperties.h"

namespace glAislinn
{
    MaterialProperties::MaterialProperties(size_t textureWidth, size_t textureHeight,
                                           Span<const uint8_t> albedo,
                                           Span<const uint8_t> roughness,
                                           Span<const uint8_t> metalness)
    {
        glGenTextures(m_textures.size(), m_textures.data());

        // set up textures:
        // Create a texture for albedo/F0
        setupTexture(textureWidth, textureHeight, albedo, 0, 3);
        // Create a texture for roughness
        setupTexture(textureWidth, textureHeight, roughness, 1, 1);
        // Create a texture for metalness
        setupTexture(textureWidth, textureHeight, metalness, 2, 1);

    }

    // we will need to keep a lot of them, so we do allow moving
    MaterialProperties::MaterialProperties(MaterialProperties&& other) noexcept : m_textures(other.m_textures)
    {
        // in the move constructor, we pillage the other one.
        // use fill to avoid hard-coding a number
        std::fill(other.m_textures.begin(), other.m_textures.end(), 0);
    }

    MaterialProperties& MaterialProperties::operator=(MaterialProperties&& other) noexcept
    {

        if(std::addressof(other) == this)
        {
            // clean up our own resources.
            glDeleteTextures(m_textures.size(), m_textures.data());

            // steal texture ids from the other one
            m_textures = other.m_textures;
            // again, calling delete on zero is fine (which will happen in the others destructor).
            // use fill to avoid hard-coding a number
            std::fill(other.m_textures.begin(), other.m_textures.end(), 0);
        }
        return *this;
    }

    MaterialProperties::~MaterialProperties()
    {
        glDeleteTextures(m_textures.size(), m_textures.data());
    }

    void MaterialProperties::setUniform(ShaderUniform uniform)
    {
        m_uniforms.push_back(uniform);
    }

    void MaterialProperties::clearUniforms()
    {
        m_uniforms.clear();
    }

    Span<const ShaderUniform> MaterialProperties::uniforms() const
    {
        return Span<const ShaderUniform>(m_uniforms);
    }

    void MaterialProperties::bindTextures(Span<const GLuint> textureUnits) const
    {
        // bind to texture units
        for(size_t tex = 0; tex < m_textures.size(); ++tex)
        {
            glActiveTexture(textureUnits[tex]);
            glBindTexture(GL_TEXTURE_2D, m_textures[tex]);
        }
    }

    void MaterialProperties::setupTexture(size_t width, size_t height, Span<const uint8_t> values, size_t buffer, size_t channels) const
    {
        // bind the first texture in the array to GL_TEXTURE_2D so we can set it up.
        glBindTexture(GL_TEXTURE_2D, m_textures[buffer]);
        // Pass the data. Albedo is RGB. We also assume that it is stored as non-Linear sRGB. We tell OpenGL this, and it will correct for us.
        if(channels == 3) glTexImage2D(GL_TEXTURE_2D, 0, GL_SRGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, values.data());
        // Roughness and metalness are 1 channel.
        else glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, width, height, 0, GL_RED, GL_UNSIGNED_BYTE, values.data());
        // Set the magnification and minification filter
        glGenerateMipmap(GL_TEXTURE_2D);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
    }
}