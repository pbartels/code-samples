//
// Created by pieterjan on 04/08/2020.
//

#include "lightpass.h"

namespace glAislinn
{
    LightPass::LightPass(unsigned int width, unsigned int height) :
                                        c_width(width), c_height(height),
                                        m_screenQuad(geometry::quad::positions, geometry::quad::normals, geometry::quad::indices, geometry::quad::uvs)
    {}

    void LightPass::draw(const GeometryBuffer& gBuffer,
                         Span<const Shader> lightShaders,
                         Span<const Light> lights,
                         Vec3f cameraPosition) const
    {
        setupGL();

        // first step: connect the geometry buffer to the shaders, so they can sample from it
        gBuffer.bindTextures(c_textureUnits);
        for(const Shader& shader: lightShaders)
        {
            shader.setUniform("position", c_textureUniforms[0]);
            shader.setUniform("normal", c_textureUniforms[1]);
            shader.setUniform("albedo", c_textureUniforms[2]);
            shader.setUniform("materialProperties", c_textureUniforms[3]);
            shader.setUniform("cameraPosition", cameraPosition);
        }

        // second step: set up each light individually, then draw a quad with it
        for(const Light& light: lights)
        {
            light.use(c_shadowTextureUnit);
            m_screenQuad.draw();
        }
    }

    void LightPass::setupGL() const
    {
        // set opengl options:
        glViewport(0, 0, c_width, c_height);
        // bind back to default framebuffer
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        // clear drawing buffer to white
        glClearColor(0.f, 0.f, 0.f, 1.0f);
        // clear color buffer (there are other buffers as well)
        glClear(GL_COLOR_BUFFER_BIT);
        // disable depth test for this one - do so explicitly as it might have been enabled in gBuffer rendering
        // also: state intent
        // Disable depth test as long as this is being rendered with a quad.
        glDisable(GL_DEPTH_TEST);
        // enable blending (we will call several shaders, for each light)
        glEnable(GL_BLEND);
        // add the results together
        glBlendFunc(GL_ONE, GL_ONE);
        // Enable gamma correction
        // The blending will happen correctly, as explained here: https://www.khronos.org/opengl/wiki/Framebuffer
        glEnable(GL_FRAMEBUFFER_SRGB);
    }
}
