//
// Created by pieterjan on 23/07/2020.
//

#include <cmath>

#include "indexedmesh.h"

namespace glAislinn
{
    IndexedMesh::IndexedMesh(Span<const float> positions,
                             Span<const float> normals,
                             Span<const unsigned int> indices,
                             Span<const float> uvs) :
            m_vao(0), m_buffers(),
            m_vertexCount(positions.size()),
            m_indexCount(indices.size()),
            m_hasUvs(!uvs.empty())
    {
        // create a VertexArrayObject
        glGenVertexArrays(1, &m_vao);
        // Bind it, so it is the currently active VAO
        glBindVertexArray(m_vao);

        // create all vertex data buffers at once
        glGenBuffers(m_buffers.size(), m_buffers.data()); // index ebo, position, normals, UVs

        //assertions on size
        if(m_vertexCount != normals.size() || (m_hasUvs && (m_vertexCount / 3 != uvs.size() / 2)))
            throw std::invalid_argument("The data passed to the IndexedMesh class does not match.");

        // stream data to the GPU, and set up connection between buffers and vertex attributes into the VAO
        loadFloats(0, positions, m_buffers[0], 3);
        loadFloats(1, normals, m_buffers[1], 3);
        if(m_hasUvs) loadFloats(2, uvs, m_buffers[2], 2);
        else glVertexAttrib2f(2, 0.5f, 0.5f);

        // EBO
        // Note that binding to this target IS saved in the VAO!
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_buffers[3]);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_indexCount * sizeof(unsigned int), indices.data(), GL_STATIC_DRAW);
    }

    // https://docs.microsoft.com/en-us/cpp/cpp/move-constructors-and-move-assignment-operators-cpp?view=vs-2019
    IndexedMesh::IndexedMesh(IndexedMesh&& other) noexcept
                                                  : m_vao(other.m_vao),
                                                    m_buffers(other.m_buffers),
                                                    m_vertexCount(other.m_vertexCount),
                                                    m_indexCount(other.m_indexCount),
                                                    m_hasUvs(other.m_hasUvs)
    {
        // Somewhat important to notice that the GLuints are basically pointers. We have to assign them to "nullptr"
        // OpenGL swallows any calls to binds/draws to those, and most importantly, calls to glDeleteBuffers
        // The "other" mesh will no longer draw anything, will be useless for any intents and purposes,
        // and it will not cause an issue when it goes out of scope:
        //      the potential issue being it destroying our data that we are still using
        other.m_vao = 0;
        // use fill to avoid hard-coding the count
        std::fill(other.m_buffers.begin(), other.m_buffers.end(), 0);
    }

    IndexedMesh& IndexedMesh::operator=(IndexedMesh&& other) noexcept
    {
        if(this != std::addressof(other))
        {
            // delete all resources controlled by this class.
            // OpenGL
            glDeleteVertexArrays(1, &m_vao);
            glDeleteBuffers(m_buffers.size(), m_buffers.data());

            // (steal) copy from other into ours
            m_vao = other.m_vao;
            m_buffers = other.m_buffers;

            // ensure other is no longer pointing to the resource
            other.m_vao = 0;
            // use fill to avoid hard-coding the count
            std::fill(other.m_buffers.begin(), other.m_buffers.end(), 0);

            // copy trivial members
            m_vertexCount = other.m_vertexCount;
            m_indexCount = other.m_indexCount;
            m_hasUvs = other.m_hasUvs;
        }
        return *this;
    }


    IndexedMesh::~IndexedMesh()
    {
        // clean up after ourselves
        glDeleteVertexArrays(1, &m_vao);
        glDeleteBuffers(m_buffers.size(), m_buffers.data());
    }


    void IndexedMesh::loadFloats(GLuint vertexAttribLocation,
                                 Span<const float> data,
                                 GLuint bufferID,
                                 unsigned int elementCount) const
    {
        // stream data to processor. The GL_ARRAY_BUFFER target is used for buffers containing vertex data
        // Important to note is that this does not actually add anything to the VAO, it just puts data on the GPU.
        glBindBuffer(GL_ARRAY_BUFFER, bufferID);
        glBufferData(GL_ARRAY_BUFFER, data.size()*sizeof(float), data.data(), GL_STATIC_DRAW);

        // now for the part where we actually put something in the VAO:
        // The VAO basically contains pointers to the buffers that explain the data in there to the vertex attributes.
        glVertexAttribPointer(vertexAttribLocation, /*vertex attrib location (see shader)*/
                              elementCount, /*number of elements per vertex attribute*/
                              GL_FLOAT, /*Type of data we are passing in*/
                              GL_FALSE, /*Whether to normalize or not*/
                              0, /*stride. Setting it to zero will make it skip 3 elements every time (see above)*/
                              nullptr /*offset into the buffer. Using nullptr instead of casting zero to a void pointer*/);

        // and then we enable the vertex attribute. Done!
        glEnableVertexAttribArray(vertexAttribLocation);
    }


    size_t IndexedMesh::vertexCount() const
    {
        return m_vertexCount;
    }

    size_t IndexedMesh::indexCount() const
    {
        return m_indexCount;
    }

    size_t IndexedMesh::triangleCount() const
    {
        return std::floor(indexCount() / 3);
    }

    bool IndexedMesh::hasUvs() const
    {
        return m_hasUvs;
    }

    void IndexedMesh::use() const
    {
        glBindVertexArray(m_vao);
    }

    void IndexedMesh::draw() const
    {
        use(); // You forgot this at first. Obviously you need to bind the vao before drawing.
        glDrawElements(GL_TRIANGLES, indexCount(), GL_UNSIGNED_INT, nullptr);
    }
}