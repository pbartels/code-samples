//
// Created by pieterjan on 24/07/2020.
//

#include "view.h"

namespace glAislinn
{
    View::View(float near, float far,
                   float right, float top,
                   Vec3f position, Quatf orientation) :
            m_near(-near), m_far(-far),
            m_right(right), m_top(top),
            m_position(std::move(position)),
            m_orientation(orientation.inverse()),
            m_viewSettingsBuffer()
    {
        // We are creating a "uniform buffer object"
        glGenBuffers(1, &m_viewSettingsBuffer); // generate a buffer, as usual.
        // we don't do anything with it yet, we just allocate it.
        // binding and transferring data is done on "use". No need to stream data we won't use.
    }

    View::View(View&& other) noexcept
                             : m_near(other.m_near), m_far(other.m_far),
                               m_right(other.m_right), m_top(other.m_top),
                               m_position(other.m_position),
                               m_orientation(other.m_orientation),
                               m_viewSettingsBuffer(other.m_viewSettingsBuffer)
    {
        // make sure the other one is no longer holding on to the gpu resource
        // We can call glDeleteBuffers on zero without a problem.
        other.m_viewSettingsBuffer = 0;
    }

    View& View::operator=(View&& other) noexcept
    {
        if(this != std::addressof(other))
        {
            // clean up our resource
            glDeleteBuffers(1, &m_viewSettingsBuffer);
            // steal the other one
            m_viewSettingsBuffer = other.m_viewSettingsBuffer;
            other.m_viewSettingsBuffer = 0;

            // copy trivial data members
            m_near = other.m_near;
            m_far = other.m_far;
            m_right = other.m_right;
            m_top = other.m_top;
            m_position = other.m_position;
            m_orientation = other.m_orientation;
        }
        return *this;
    }

    View::~View()
    {
        glDeleteBuffers(1, &m_viewSettingsBuffer);
    }

    Mat4f View::getViewTransform() const
    {
        return getPerspectiveProjection() * getRotation() * getTranslation();
    }

    Vec3f View::position() const
    {
        return m_position;
    }

    void View::setPosition(Vec3f position)
    {
        m_position = std::move(position);
    }

    void View::movePosition(const Vec3f& delta)
    {
        m_position += delta;
    }

    Quatf View::orientation() const
    {
        return m_orientation;
    }

    void View::setOrientation(Quatf orientation)
    {
        m_orientation = std::move(orientation);
    }

    void View::setOrientation(const AngleAxisf& orientation)
    {
        m_orientation = Quatf(orientation);
    }


    void View::lookAt(const Vec3f& lookat, Vec3f up)
    {
        Vec3f view = m_position - lookat;
        view.normalize();

        up.normalize();
        Vec3f right = up.cross(view);
        right.normalize();
        up = view.cross(right);
        up.normalize();

        Mat3f rot; rot << right, up, view;

        // don't invert this: you are defining this transform as how to move the View into the right position.
        m_orientation = Quatf(rot.transpose());
    }

    void View::sync(GLuint bindPointNumber) const
    {
        // stream data to the uniform buffer
        // At the same time, we point this buffer to the view settings bind point
        glBindBufferBase(GL_UNIFORM_BUFFER, bindPointNumber, m_viewSettingsBuffer);
        glBufferData(GL_UNIFORM_BUFFER, 16 * sizeof(float), getViewTransform().data(), GL_DYNAMIC_READ);
    }

    Mat4f View::getPerspectiveProjection() const
    {
        // As in RTR chapter 4

        Mat4f result = Mat4f::Zero();

        // top row
        result(0, 0) = -m_near / m_right;

        // second row
        result(1, 1) = -m_near / m_top;

        // third row
        result(2, 2) = 1.f * (m_far + m_near) / (m_near - m_far);
        result(2, 3) = -2.f * (m_far * m_near) / (m_near - m_far);

        // final row
        result(3, 2) = -1.f;

        return result;
    }


    Mat4f View::getRotation() const
    {
        return Affine3f(m_orientation).matrix();
    }

    Mat4f View::getTranslation() const
    {
        return Affine3f(Translation3f(-1 * m_position)).matrix();
    }


}