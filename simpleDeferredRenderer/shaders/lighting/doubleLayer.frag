#version 330 core

in vec2 uv;

// Define a uniform block that takes in the shadow view.
// Doing this with a uniform buffer object allows us to share the same view between different shader programs,
// and to swap out the view quickly (because all the memory is already on the GPU).
// This is buffer backed and HAS to be provided
uniform ShadowView
{
    mat4 transform;
} shadowview;

uniform sampler2D position;
uniform sampler2D normal;
uniform sampler2D albedo;
uniform sampler2D materialProperties;
uniform sampler2D shadow;

uniform vec3 cameraPosition;
uniform vec3 lightPosition;
uniform vec4 lightIntensity;
uniform vec4 dielectricBaseSpec = vec4(0.04, 0.04, 0.04, 1);

// two-layer model parameters
uniform float n1 = 1.0;
uniform float n2 = 1.33;
uniform float topRoughness = 0.1f;
uniform float topThickness = 1.f;
uniform vec4 absorptionCoefficient = vec4(0.1, 0.3, 0.3, 1.0);

const float PI = 3.14159265358979323846;

out vec4 fragColor;

float shadowMultiplier(vec3 fragPosition, float cosLight)
{
    vec4 perspLightSpacePosition = shadowview.transform * vec4(fragPosition, 1);
    perspLightSpacePosition = perspLightSpacePosition / perspLightSpacePosition.w;
    vec3 lightSpacePosition = 0.5 + 0.5 * (perspLightSpacePosition.xyz);
    float shadowDepth = texture(shadow, lightSpacePosition.xy).r;

    float bias = 0.005f;
    if(shadowDepth >= 1.f - 0.1 * bias) return 1.f;
    float fragDepth = clamp(shadowDepth - lightSpacePosition.z + bias, 0.f, bias);
    return smoothstep(0.f, 1.f, fragDepth / 0.005f);
}

vec3 refract(vec3 normal, vec3 light, float n)
{
    float w = n * dot(normal, light);
    // kind of cheating the critical angle here
    float k = sqrt(max(1 + (w - n) * (w + n), 0));

    return normalize(((w-k) * normal) - (n * light));
}

vec4 F(vec4 baseSpec, float cosLight)
{
    // schlick
    return baseSpec + (1 - baseSpec) * pow(1 - clamp(cosLight, 0, 1), 5);
}

float D(float roughness, vec3 normal, vec3 H)
{
    // GGX as defined by Disney
    float dotHNSq = pow(dot(H, normal), 2);
    float rSq = pow(roughness, 2);


    float num = rSq / PI;
    float denominator = dotHNSq * (rSq + ((1 - dotHNSq)/ dotHNSq));

    return num / (denominator * denominator);
}

float G(float roughness, float cosLight, float cosViewer)
{
    // GGX Smith as defined by Disney
    float mu_i = cosLight * cosLight;
    float mu_o = cosViewer * cosViewer;
    float rSq = roughness * roughness;

    float t1 = cosLight + sqrt(rSq + mu_i - (rSq * mu_i));
    float t2 = cosViewer + sqrt(rSq + mu_o - (rSq * mu_o));

    return 1 / ((t1 * t2) + 0.001);
}

void main()
{
    // Object properties
    vec3 fragPosition = texture(position, uv).xyz;
    vec3 fragNormal = normalize(texture(normal, uv).xyz);
    vec4 fragAlbedo = texture(albedo, uv);
    vec4 fragProperties = texture(materialProperties, uv);
    float fragRoughness = fragProperties.r;
    float fragMetalness = fragProperties.g;
    float useLayer = fragProperties.b;

    // get direction vectors
    // Light
    vec3 wLight = lightPosition - fragPosition;
    float lightDistance = length(wLight);
    wLight = normalize(wLight);
    float cosLight = dot(fragNormal, wLight);
    // View
    vec3 wViewer = normalize(cameraPosition - fragPosition);
    float cosViewer = dot(fragNormal, wViewer);
    // Half vector
    vec3 H = normalize(wLight + wViewer);
    float cosHL = dot(H, wLight);
    float cosHV = dot(H, wViewer);

    // get light quantities
    float distanceFalloff = pow(1.0 / lightDistance, 2);
    vec4 arrivingLightIntensity = distanceFalloff * lightIntensity;

    /* Top layer */
    // we assume the top layer is a dielectric
    vec4 fresnelTopLayer = F(dielectricBaseSpec, cosHL);
    vec4 refractedEnergyIn = (vec4(1) - fresnelTopLayer) * pow(n2 / n1, 2);
    vec4 topLayerBRDF = fresnelTopLayer * D(topRoughness, fragNormal, H) * G(topRoughness, cosLight, cosViewer);
    topLayerBRDF = mix(vec4(0, 0, 0, 1), topLayerBRDF, useLayer);

    /* Bottom layer */
    vec3 lightRefracted = mix(wLight, -refract(H, wLight, n1 / n2), useLayer);
    vec3 viewRefracted = mix(wViewer, -refract(H, wViewer, n2 / n1), useLayer);
    vec3 HRefracted = normalize(lightRefracted + viewRefracted);
    float cosHLRefracted = dot(lightRefracted, HRefracted);
    float cosHVRefracted = dot(viewRefracted, HRefracted);
    float cosLRefracted = dot(lightRefracted, fragNormal);
    float cosVRefracted = dot(viewRefracted, fragNormal);

    // Here we use the textured values, and we allow metallic materials
    vec4 baseSpec = mix(dielectricBaseSpec, fragAlbedo, fragMetalness);
    vec4 bottomDiffuseBRDF = mix(fragAlbedo, vec4(0, 0, 0, 1), fragMetalness) / PI;
    float bottomG = G(fragRoughness, cosLRefracted, cosVRefracted);
    vec4 bottomSpecularBRDF = F(baseSpec, cosHLRefracted) *
    D(fragRoughness, fragNormal, HRefracted) * bottomG;
    bottomG *= 4 * cosLRefracted * cosVRefracted;

    vec4 bottomLayerBRDF = bottomDiffuseBRDF + bottomSpecularBRDF;

    /* bottom layer attenuation */
    vec4 fresnelTopLayerExit = F(dielectricBaseSpec, cosHVRefracted);
    vec4 refractedEnergyOut = (vec4(1) - fresnelTopLayerExit) * pow(n1 / n2, 2);
    refractedEnergyOut = refractedEnergyOut;
    vec4 absorption = exp(-absorptionCoefficient * topThickness * ((1 / cosHLRefracted) + (1 / cosHVRefracted)));
    vec4 attenuation = mix(vec4(1), absorption * refractedEnergyOut * refractedEnergyIn, useLayer);

    /* total brdf */
    vec4 totalBRDF = topLayerBRDF + attenuation * bottomLayerBRDF;

    fragColor = shadowMultiplier(fragPosition, cosLight) * totalBRDF * cosLight * arrivingLightIntensity;
}
