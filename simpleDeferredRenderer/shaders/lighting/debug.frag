#version 330 core

in vec2 uv;


// Define a uniform block that takes in the view.
// Doing this with a uniform buffer object allows us to share the same view between different shader programs,
// and to swap out the view quickly (because all the memory is already on the GPU).
// This is buffer backed and HAS to be provided
uniform ShadowView
{
    mat4 transform;
} shadowview;

uniform sampler2D position;
uniform sampler2D normal;
uniform sampler2D albedo;
uniform sampler2D materialProperties;
uniform sampler2D shadow;

uniform int selectOutput = 0;

out vec4 fragColor;

vec4 calculateShadowMap()
{
    vec4 wsPosition = texture(position, uv);
    vec4 perspLightSpacePosition = shadowview.transform * wsPosition;
    perspLightSpacePosition = perspLightSpacePosition / perspLightSpacePosition.w;
    vec3 lightSpacePosition = 0.5 + 0.5 * (perspLightSpacePosition.xyz);
    float shadowDepth = texture(shadow, lightSpacePosition.xy).r;
    float fragDepth = lightSpacePosition.z - 0.01;

    return vec4(fragDepth - shadowDepth > 0  ? 1 : 0);
}

void main()
{
    switch(selectOutput)
    {
        case 0: fragColor = texture(position, uv); break;
        case 1: fragColor = normalize(texture(normal, uv)); break;
        case 2: fragColor = texture(albedo, uv); break;
        case 3: fragColor = texture(materialProperties, uv); break;
        case 4: fragColor = calculateShadowMap(); break;
    }
}
