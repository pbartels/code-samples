#version 330 core

layout (location = 0) in vec3 position;
// So this is interesting: Drawable specify normals but this shader doesn't need them
// I can disable them and it keeps working. It seems like that's supposed to happen, which makes sense to me.
// However, especially in older drivers, it could crash. If I were to ever ship this, I should fix it.
// furthermore, it would be cleaner to have separate VAOs for lighting passes.
//layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 uvIn;

out vec2 uv;

void main()
{
    gl_Position = vec4(position, 1.0);
    uv = uvIn;
}
