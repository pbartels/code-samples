#version 330 core

in vec2 uv;

// Define a uniform block that takes in the shadow view.
// Doing this with a uniform buffer object allows us to share the same view between different shader programs,
// and to swap out the view quickly (because all the memory is already on the GPU).
// This is buffer backed and HAS to be provided
uniform ShadowView
{
    mat4 transform;
} shadowview;

uniform sampler2D position;
uniform sampler2D normal;
uniform sampler2D albedo;
uniform sampler2D materialProperties;
uniform sampler2D shadow;

uniform vec3 cameraPosition;
uniform vec3 lightPosition;
uniform vec4 lightIntensity;
uniform vec4 dielectricBaseSpec = vec4(0.04, 0.04, 0.04, 1);

const float PI = 3.14159265358979323846;

out vec4 fragColor;

float shadowMultiplier(vec3 fragPosition, float cosLight)
{
    vec4 perspLightSpacePosition = shadowview.transform * vec4(fragPosition, 1);
    perspLightSpacePosition = perspLightSpacePosition / perspLightSpacePosition.w;
    vec3 lightSpacePosition = 0.5 + 0.5 * (perspLightSpacePosition.xyz);
    float shadowDepth = texture(shadow, lightSpacePosition.xy).r;

    float bias = 0.005f;
    if(shadowDepth >= 1.f - 0.1 * bias) return 1;
    float fragDepth = clamp(shadowDepth - lightSpacePosition.z + bias, 0.f, bias);
    return smoothstep(0, 1, fragDepth / 0.005f);
}

vec4 F(vec4 baseSpec, float cosLight)
{
    // schlick
    return baseSpec + (1 - baseSpec) * pow(1 - clamp(cosLight, 0, 1), 5);
}

float D(float roughness, vec3 normal, vec3 H)
{
    // GGX as defined by Disney
    float dotHNSq = pow(dot(H, normal), 2);
    float rSq = pow(roughness, 2);


    float num = rSq / PI;
    float denominator = dotHNSq * (rSq + ((1 - dotHNSq)/ dotHNSq));

    return num / (denominator * denominator);
}

float G(float roughness, float cosLight, float cosViewer)
{
    // GGX Smith as defined by Disney
    float mu_i = cosLight * cosLight;
    float mu_o = cosViewer * cosViewer;
    float rSq = roughness * roughness;

    float t1 = cosLight + sqrt(rSq + mu_i - (rSq * mu_i));
    float t2 = cosViewer + sqrt(rSq + mu_o - (rSq * mu_o));

    return 1 / ((t1 * t2) + 0.001);
}

void main()
{
    // Object properties
    vec3 fragPosition = texture(position, uv).xyz;
    vec3 fragNormal = normalize(texture(normal, uv).xyz);
    vec4 fragAlbedo = texture(albedo, uv);
    vec4 fragProperties = texture(materialProperties, uv);
    float fragRoughness = fragProperties.r;
    float fragMetalness = fragProperties.g;

    // get direction vectors
    vec3 wLight = lightPosition - fragPosition;
    float lightDistance = length(wLight);
    wLight = normalize(wLight);
    float cosLight = dot(fragNormal, wLight);
    vec3 wViewer = normalize(cameraPosition - fragPosition);
    float cosViewer = dot(fragNormal, wViewer);
    vec3 H = normalize(wLight + wViewer);
    float cosHL = dot(H, wLight);
    float cosHV = dot(H, wViewer);

    // get light quantities
    float distanceFalloff = pow(1.0 / lightDistance, 2);
    vec4 arrivingLightIntensity = distanceFalloff * lightIntensity;

    // No diffuse term for Conductors
    vec4 diffuseBRDF = mix(fragAlbedo, vec4(0, 0, 0, 1), fragMetalness) / PI;

    vec4 baseSpec = mix(dielectricBaseSpec, fragAlbedo, fragMetalness);
    vec4 specularBRDF = F(baseSpec, cosHL) * D(fragRoughness, fragNormal, H) * G(fragRoughness, cosLight, cosViewer);

    fragColor = shadowMultiplier(fragPosition, cosLight) * (diffuseBRDF + specularBRDF) * cosLight * arrivingLightIntensity;
}
