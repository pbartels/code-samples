#version 330 core

// reading in vertex data
in vec3 wsPosition;
in vec3 wsNormal;
in vec2 uv;

// reading in material properties
uniform sampler2D albedo;
uniform sampler2D roughness;
uniform sampler2D metalness;

// uniform material properties
uniform float useTopLayer;

// multiple render targets
layout (location = 0) out vec4 FragPosition;
layout (location = 1) out vec4 FragNormal;
layout (location = 2) out vec4 FragAlbedo;
layout (location = 3) out vec4 FragMaterial;

void main() {
    // forwarding world space positions and normals to the lighting pass
    FragPosition = vec4(wsPosition, 1);
    FragNormal = vec4(normalize(wsNormal), 1);
    // texture look ups
    FragAlbedo = texture(albedo, uv);
    vec4 alpha = texture(roughness, uv);
    vec4 metal = texture(metalness, uv);
    FragMaterial = vec4(alpha.r, metal.r, useTopLayer, 1);
}
