#version 330 core

layout (location = 0) in vec3 position;

// Define a uniform block that takes in the view.
// Doing this with a uniform buffer object allows us to share the same view between different shader programs,
// and to swap out the view quickly (because all the memory is already on the GPU).
// This is buffer backed and HAS to be provided
uniform View
{
    mat4 transform;
} view;

struct Model
{
    mat4 transform;
    mat3 normalTransform;
};

// You can specify uniform default in the shader itself
uniform Model model = Model(mat4(1.0), mat3(1.0));

void main()
{
    gl_Position = view.transform * model.transform * vec4(position, 1);
}