#version 330 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 uvIn;

// Define a uniform block that takes in the view.
// Doing this with a uniform buffer object allows us to share the same view between different shader programs,
// and to swap out the view quickly (because all the memory is already on the GPU).
// This is buffer backed and HAS to be provided
uniform View
{
    mat4 transform;
} view;

struct Model
{
    mat4 transform;
    mat3 normalTransform;
};

// You can specify uniform default in the shader itself
uniform Model model = Model(mat4(1.0), mat3(1.0));


out vec3 wsPosition;
out vec3 wsNormal;
out vec2 uv;

void main() {
    // "pass through" shader:
    // defining these outputs to ensure they will be interpolated and sent to the fragment shader.
    // we normalize the normal before interpolation (and after, see RTR)
    // although it would be more performant to expect normalization on the CPU side, but that's not enforceable.
    wsPosition = (model.transform * vec4(position, 1.0)).xyz;
    wsNormal = normalize(model.normalTransform * normalize(normal));
    uv = uvIn;

    // and set the NDC position
    gl_Position = view.transform * vec4(wsPosition, 1.0);
}
