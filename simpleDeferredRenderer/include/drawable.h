//
// Created by pieterjan on 25/07/2020.
//

#ifndef AISLINN_DRAWABLE_H
#define AISLINN_DRAWABLE_H

#include <memory>

#include "base.h"
#include "indexedmesh.h"
#include "materialproperties.h"
#include "shader.h"

namespace glAislinn
{
    /*
     * A class representing a collection of information that's drawable:
     *       A reference to VertexData, material properties and model transforms.
     * Notice that the material properties, which will be a separate class, are (logically) not part of the Shader, but of this class
     */
    class Drawable
    {
    public:
        Drawable(const IndexedMesh& mesh,
                 const MaterialProperties& materialProperties,
                 Affine3f transform = Affine3f::Identity());

        // can be copied and moved just fine. Defaults are okay.

        // move the thing around. Affine3f syntax doesn't allow implicit casting, which is clunky.
        void setTransform(Affine3f transform);
        void applyTransform(Affine3f transform);
        void preTransform(Affine3f transform);
        void clearTransform();

        // We want to pass our model transform and material values to the shader before rendering
        void draw(const Shader& shader, Span<const GLuint> materialTextureUnits) const;
        // Sometimes we only want to pass the model transform
        void draw(const Shader& shader) const;

    private:
        // any transform needed to transform the vertex data from model space to world space
        // Will be passed into the shader as model.transform
        Affine3f m_modelTransform;
        // and model.normalTransform
        Mat3f m_normalTransform;

        // A big question is: how do we reference the IndexedMesh here?
        // Options are:
        //      * A raw pointer/reference, as it does not have ownership of the IndexedMesh, but this risks dangling pointers
        //          On the plus side, it is more flexible, as it accepts any IndexedMesh:
        //             The other two options force the user of the class to adopt smart pointers
        //          Which also means it forces them to put the IndexedMeshes on the heap.
        //          Whereas here we can still use the stack, depending on the application.
        //      * A shared pointer, which automates memory management, but implies ownership
        //      * A weak pointer, which does not imply ownership, but lets us handle a dangling pointer gracefully.
        //
        // I chose std::reference_wrapper because of the added flexibility,
        //      In typical aislinn use cases, VertexData will not be thrown out before the drawables are (lifetime)
        // Furthermore, the std::reference_wrapper expresses intent (no ownership) more clearly than a raw pointer
        //  The calling syntax is slightly nicer and it can never be null.
        std::reference_wrapper<const IndexedMesh> m_vertexData;
        // Same goes for the MaterialProperties:
        std::reference_wrapper<const MaterialProperties> m_materialProperties;
    };
}

#endif //AISLINN_DRAWABLE_H
