//
// Created by pieterjan on 30/07/2020.
//

#ifndef AISLINN_MATERIALPROPERTIES_H
#define AISLINN_MATERIALPROPERTIES_H

#include <array>
#include <map>

#include "base.h"
#include "shader.h"

namespace glAislinn
{
    class MaterialProperties
    {
    public:
        MaterialProperties(size_t textureWidth, size_t textureHeight,
                           Span<const uint8_t> albedo,
                           Span<const uint8_t> roughness,
                           Span<const uint8_t> metalness);

        // same logic as before, this encapsulates GPU resources: non copyable
        MaterialProperties(const MaterialProperties&) = delete;
        MaterialProperties& operator=(const MaterialProperties&) = delete;

        // we will need to keep a lot of them, so we do allow moving
        MaterialProperties(MaterialProperties&&) noexcept;
        MaterialProperties& operator=(MaterialProperties&&) noexcept;

        ~MaterialProperties();

        void setUniform(ShaderUniform);
        void clearUniforms();
        Span<const ShaderUniform> uniforms() const;

        void bindTextures(Span<const GLuint> textureUnits) const;
    private:
        void setupTexture(size_t width, size_t height, Span<const uint8_t> values, size_t buffer, size_t channels) const;

        // Currently we have three maps: Albedo (diffuse color/F0), roughness, metalness
        // Eventually we will add normals, displacement
        std::array<GLuint, 3> m_textures;
        std::vector<ShaderUniform> m_uniforms;
    };
}

#endif //AISLINN_MATERIALPROPERTIES_H
