//
// Created by pieterjan on 23/07/2020.
//

#ifndef AISLINN_INDEXEDMESH_H
#define AISLINN_INDEXEDMESH_H

#include <array>

#include "base.h"
#include "glad.h"

namespace glAislinn
{
    /**
     *
     * a mesh residing in GPU memory (in a VAO)
     * It expects at least positions, normals and indices to make triangles out of them
     * UVs are optional
     *
     * There is an open question here: allow copies or not?
     * Since the memory resides on the GPU, copies on the CPU (in our code) are cheap.
     *      Allows for easy instancing
     * Allowing copies does mean that the memory can't be deleted from the GPU anymore
     *      (because there's not reference counting)
     * For now I've decided against allowing copying. This lets us free up the memory buffers on deletion.
     *      This will potentially add some slight overhead on the CPU side, but who cares about that :-D
     *      On a more serious note, it will probably mean we have to work with shared pointers in some potential scene class (to allow instancing)
     *      Or something simpler, like keeping indices, and invalidating them. Reference counting would be cleaner.
     *
     */
    class IndexedMesh
    {
    public:
        IndexedMesh(Span<const float> positions,
                    Span<const float> normals,
                    Span<const unsigned int> indices,
                    Span<const float> uvs = Span<const float>()); // empty span uses nullptr

        // Core guidelines: C.21 state intent by deleting *all* default operations
        IndexedMesh(const IndexedMesh &) = delete;
        IndexedMesh operator=(const IndexedMesh &) = delete;

        // Moving is allowed: all GPU memory handles are set to zero.
        IndexedMesh(IndexedMesh&&) noexcept;
        // This allows a mesh to be swapped. Using this means that all instances drawing this mesh will suddenly change.
        IndexedMesh& operator=(IndexedMesh&&) noexcept;

        // declare custom destructor: free up GPU memory.
        ~IndexedMesh();

        size_t vertexCount() const;
        size_t indexCount() const;
        size_t triangleCount() const;
        bool hasUvs() const;

        void use() const;
        void draw() const;

    private:
        void loadFloats(GLuint vertexAttribLocation,
                        Span<const float> data,
                        GLuint bufferID,
                        unsigned int elementCount) const;

        GLuint m_vao;
        std::array<GLuint, 4> m_buffers; // positions, normals, uvs, EBO (indices)
        size_t m_vertexCount;
        size_t m_indexCount;
        bool m_hasUvs;
    };
}


#endif //AISLINN_INDEXEDMESH_H