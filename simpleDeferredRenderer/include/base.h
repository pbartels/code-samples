//
// Created by pbartels on 7/24/20.
//

#ifndef AISLINN_BASE_H
#define AISLINN_BASE_H

#include <cmath>

#include <gsl/span>

#include <eigen3/Eigen/Eigen>

#include "glad.h"

namespace glAislinn
{
    // We use gsl::span to refer to a contiguous block of memory
    // it allows making abstraction of how, where and by who the data was allocated.
    // it does NOT take ownership of the data whatsoever.
    // it replaces the (T*, size_t) way of passing around data.
    // Note that you can assign a std::vector straight to a span, and it works nicely and easily.
    template<typename datatype> using Span = gsl::span<datatype>;

    // the bind point on the GL_UNIFORM_BUFFER target to use for viewer settings.
    // By defining this globally, we ensure that this correctly set everywhere
    // constexpr means it is evaluated it at compile time, making it okay to define it in a header
    constexpr GLuint viewSettingsUniformBlockBindPoint = 0;
    // use a different bind point for the shadow view(s)
    constexpr GLuint shadowViewSettingsUniformBlockBindPoint = 1;

    constexpr unsigned int shadowMapResolution = 1024;

    // define pi
    constexpr float pi = M_PI;

    /*
     * Renaming some core eigen types.
     * The main idea of renaming them is so that replacing this one day would be easier
     *  and also that it looks neater to not have "Eigen::" all over the code.
     *
     * I'm not sure how easy actually replacing all this would be, as it's just a wrapper around the typenames, not the API
     */
    using Vec2f = Eigen::Vector2f;
    using Vec3f = Eigen::Vector3f;
    using Vec4f = Eigen::Vector4f;

    using Mat2f = Eigen::Matrix2f;
    using Mat3f = Eigen::Matrix3f;
    using Mat4f = Eigen::Matrix4f;

    using Vec2d = Eigen::Vector2d;
    using Vec3d = Eigen::Vector3d;
    using Vec4d = Eigen::Vector4d;

    using Mat2d = Eigen::Matrix2d;
    using Mat3d = Eigen::Matrix3d;
    using Mat4d = Eigen::Matrix4d;

    using Vec2i = Eigen::Vector2i;
    using Vec3i = Eigen::Vector3i;
    using Vec4i = Eigen::Vector4i;

    using Mat2i = Eigen::Matrix2i;
    using Mat3i = Eigen::Matrix3i;
    using Mat4i = Eigen::Matrix4i;

    using Quatf = Eigen::Quaternionf;
    using AngleAxisf = Eigen::AngleAxisf;
    using Affine3f = Eigen::Affine3f;
    using Translation3f = Eigen::Translation3f;
    using Scale3f = Eigen::AlignedScaling3f;
    using UniformScale3f = Eigen::UniformScaling<float>;

    using Quatd = Eigen::Quaterniond;
    using AngleAxisd = Eigen::AngleAxisd;
    using Affine3d = Eigen::Affine3d;
    using Translation3d = Eigen::Translation3d;
    using Scale3d = Eigen::AlignedScaling3d;
}

#endif //AISLINN_BASE_H
