//
// Created by pbartels on 8/9/20.
//

#ifndef AISLINN_SHADOWSHADER_H
#define AISLINN_SHADOWSHADER_H

#include "base.h"
#include "shader.h"

namespace glAislinn
{
    class ShadowShader : public Shader
    {
    public:
        // inherit constructors from Shader
        using Shader::Shader;

        // Hide the base constructor, re-use everything else.
        ShadowShader(const std::string & vertexShaderSourceFile,
                     const std::string & fragmentShaderSourceFile);
    };
}

#endif //AISLINN_SHADOWSHADER_H
