//
// Created by pieterjan on 28/07/2020.
//

#ifndef AISLINN_GEOMETRYBUFFER_H
#define AISLINN_GEOMETRYBUFFER_H

#include "glad.h"

#include "base.h"
#include "drawable.h"
#include "shader.h"

namespace glAislinn
{
    class GeometryBuffer
    {
    public:
        // Allocate the geometry buffer. RAII!
        // See the definition for more information.
        GeometryBuffer(unsigned int width, unsigned int height);

        // non-copyable, non-movable: GPU-side resource wrapper
        // State intent by deleting all
        GeometryBuffer(const GeometryBuffer&) = delete;
        GeometryBuffer(GeometryBuffer&&) = delete;
        GeometryBuffer& operator=(const GeometryBuffer&) noexcept = delete;
        GeometryBuffer& operator=(GeometryBuffer&&) noexcept = delete;

        // We need to clean up the GPU side resources when destroying the geometry buffer
        ~GeometryBuffer();

        void draw(Span<const Drawable> drawables) const;

        void bindTextures(Span<const GLenum> textureUnits) const;

    private:
        const unsigned int c_width;
        const unsigned int c_height;
        GLuint m_framebuffer;
        std::array<GLuint, 5> m_textures; // Currently: positions, normals, albedo, material properties and depth
        Shader m_gBufferShader;
        // static set of render targets to use for multiple render target assignment (this is the gbuffer output)
        // this avoids having this hard-coded in the cpp code. This way it's visible.
        constexpr static std::array<GLenum, 4> c_renderTargets = {GL_COLOR_ATTACHMENT0,
                                                                  GL_COLOR_ATTACHMENT1,
                                                                  GL_COLOR_ATTACHMENT2,
                                                                  GL_COLOR_ATTACHMENT3};

        // static set of Texture units to use for rendering the material properties INTO the gbuffer (this is gbuffer input)
        constexpr static std::array<GLenum, 3> c_materialTextureUnits = {GL_TEXTURE0,
                                                                         GL_TEXTURE1,
                                                                         GL_TEXTURE2};
        constexpr static std::array<int, 3> c_materialTextureUniforms = {0, 1, 2};
    };
}

#endif //AISLINN_GEOMETRYBUFFER_H
