//
// Created by pieterjan on 24/07/2020.
//

#ifndef AISLINN_SHADER_H
#define AISLINN_SHADER_H

#include <functional>
#include <string>
#include <variant>

#include "glad.h"

#include "base.h"

namespace glAislinn
{
    class ShaderUniform
    {
    public:
        using ValueType = std::variant<float, int, Vec3f, Vec4f, Mat3f, Mat4f>;

        ShaderUniform(std::string name, ValueType value);

        const std::string& name() const;
        ValueType value() const;

    private:
        std::string m_name;
        std::variant<float, int, Vec3f, Vec4f, Mat3f, Mat4f> m_value;
    };

    /**
     * A wrapper class around GLSL shaders.
     *
     * Copying is forbidden. See IndexedMesh for a rationale.
     *
     * In the source code you will see that shaders and programs have a wholly different calling convention:
     *      There is no binding, the program id is just *always* passed as a parameter
     *      but then when we want to use it, we have to "bind" it by calling glUseProgram.
     *
     */
    class Shader
    {
    public:
        Shader(const std::string & vertexShaderSourceFile,
               const std::string & fragmentShaderSourceFile);

        // Core guidelines: C.21 state intent by deleting *all* default operations
        Shader(const Shader &)=delete; // Same logic as in IndexedMesh: avoiding copies allows us to clean up GPU memory on deletion.
        Shader operator=(const Shader&)=delete;

        // Move constructor
        Shader(Shader&&) noexcept;
        // it feels weird having a shader be move assignable.
        // What would the use case for that be?
        Shader& operator=(Shader&&) = delete;

        void use() const;

        // declare custom destructor: free GPU memory.
        virtual ~Shader();

        // hiding the horrible api by overloading these functions.
        // these functions DO NOT error when a uniform was not found, they return false
        // I prefer it this way (for now), because it allows me to just set uniforms on any kind of shader,
        // regardless of whether the shader supports it or not.
        bool setUniform(const std::string & uniformName, float value) const;
        bool setUniform(const std::string & uniformName, int value) const;
        bool setUniform(const std::string & uniformName, Vec3f value) const;
        bool setUniform(const std::string & uniformName, Vec4f value) const;
        bool setUniform(const std::string & uniformName, Mat3f value) const;
        bool setUniform(const std::string & uniformName, Mat4f value) const;
        bool setUniform(ShaderUniform uniform) const;

        bool hasViewSupport() const;

    protected:
        // This is used to change the shader to use a different bind point. This is specifically useful for rendering depth maps.
        // This way the "light views" use a different paths than the camera view, which leaves the latter untouched.
        void setCameraViewBindPoint(GLuint bindPointNumber);
        bool setViewBindPoint(const std::string& uniformBlockName, GLuint bindPointNumber);
    private:
        std::string readFile(const std::string & filename) const;
        GLuint compileShader(const std::string & sourceFilename, GLenum shaderType) const;
        GLint uniformLocation(const std::string & uniformName) const;


        GLuint m_program;
        bool m_hasViewSupport;

    };
}

#endif //AISLINN_SHADER_H
