//
// Created by pieterjan on 23/07/2020.
//

#ifndef AISLINN_DEVICE_H
#define AISLINN_BACKEND_H

#include <memory>

#include "glad.h"
#include <GLFW/glfw3.h>

#include "base.h"

namespace glAislinn
{

    // user control values
    struct UserControl
    {
        double mousePositionX, mousePositionY;
        double mouseDeltaX, mouseDeltaY;
        Vec2f arrows;
    };

    class Backend
    {
    public:
        Backend(int screenWidth, int screenHeight);

        // this also deletes the implicit move constructor:
        //  https://stackoverflow.com/questions/37276413/default-move-constructor-assignment-and-deleted-copy-constructor-assignment
        // but:
        // Core guidelines: C.21 state intent by deleting *all* default operations
        Backend(const Backend &) = delete;
        Backend operator=(const Backend&) = delete;
        Backend(Backend&&) noexcept = delete;
        Backend& operator=(Backend&&) noexcept = delete;

        ~Backend();


        bool keepRendering() const;

        UserControl startRenderLoop();

        void finishRenderLoop() const;

        double getTimeInSeconds() const;

        UserControl userControl() const;

    private:
        void processInputs();

        // This clearly implies that this class is *not* the window owner
        // I'm not sure if that's a correct assumption, but the glfw api *really* seems to imply it is
        GLFWwindow* m_window;

        UserControl m_userControlValues;
    };

}

#endif //AISLINN_DEVICE_H
