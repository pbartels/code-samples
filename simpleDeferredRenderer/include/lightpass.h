//
// Created by pieterjan on 04/08/2020.
//

#ifndef AISLINN_LIGHTPASS_H
#define AISLINN_LIGHTPASS_H

#include <array>

#include "geometrybuffer.h"
#include "indexedmesh.h"
#include "light.h"
#include "shader.h"

#include "quad.h"

namespace glAislinn
{
    /**
     * Lightweight class that wraps around the basic light pass settings.
     */
    class LightPass
    {
    public:
        LightPass(unsigned int width, unsigned int height);

        // non-copyable, non-movable. Explicitly stating intent.
        LightPass(const LightPass &) = delete;
        LightPass(LightPass&&) noexcept = delete;
        LightPass& operator=(const LightPass &) = delete;
        LightPass& operator=(LightPass&&) noexcept = delete;

        // we take spans to give the user flexibility!
        void draw(const GeometryBuffer& gBuffer,
                  Span<const Shader> lightShaders,
                  Span<const Light> lights,
                  Vec3f cameraPosition) const;
    private:
        void setupGL() const;

        const unsigned int c_width, c_height;
        IndexedMesh m_screenQuad;

        // these and the ones below have to match.
        constexpr static std::array<int, 4> c_textureUniforms = {0, /* Position */
                                                                 1, /* Normal */
                                                                 2, /* Albedo */
                                                                 3};; /* Material */

        constexpr static std::array<GLenum, 4> c_textureUnits = {GL_TEXTURE0, /* Position */
                                                                 GL_TEXTURE1, /* Normal */
                                                                 GL_TEXTURE2, /* Albedo */
                                                                 GL_TEXTURE3}; /* Material */

        constexpr static GLenum c_shadowTextureUnit = GL_TEXTURE4;
    };
}

#endif //AISLINN_LIGHTPASS_H
