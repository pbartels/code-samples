//
// Created by pieterjan on 04/08/2020.
//

#ifndef AISLINN_LIGHT_H
#define AISLINN_LIGHT_H

#include "base.h"
#include "drawable.h"
#include "shader.h"
#include "shadowshader.h"
#include "view.h"

namespace glAislinn
{
    /**
     * Base class for all types of lights.
     *
     * Currently, this represents a simple point light.
     *
     * It takes a reference to a shader, it does *not* own a shader.
     * It is allowed to check beforehand if the given shader support all of its features, and throw an exception if not.
     *
     * This has a few advantages, as noted below.
     */
    class Light
    {
    public:
        Light(const Shader& shader,
              const ShadowShader& shadowShader,
              Vec3f position,
              Vec4f intensity = Vec4f{1.f, 1.f, 1.f, 1.f},
              Vec4f viewProperties = Vec4f{0.5f, 4.f, 0.75f, 0.75f},
              Vec3f target = Vec3f{0.f, 0.f, 0.f},
              Vec3f up = Vec3f{0.f,1.f, 0.f});

        // Non-copyable (View data member);
        Light(const Light &) = delete;
        Light& operator=(const Light&) = delete;
        // Movable. Defaults no longer okay.
        Light(Light&&) noexcept;
        Light& operator=(Light&&) noexcept;

        // default no longer okay
        ~Light();

        void setPosition(Vec3f position);
        void move(Vec3f delta);
        Vec3f position() const;

        void setOrientation(Vec3f target, Vec3f up = {0, 1, 0});
        Quatf orientation() const;

        void setIntensity(Vec4f intensity);
        Vec4f intensity() const;

        const Shader& shader() const;

        // Function to fill the shadow map texture for this point light.
        void drawShadowMap(Span<const Drawable> drawables) const;

        // this function sets all the relevant uniforms on the shader
        // Only this class is supposed to know how to do this
        // It is allowed to check beforehand (in the constructor) if the given shader supports its type.
        void use(GLenum shadowTextureUnit) const;

    private:
        void syncView() const;

        // This allows the developer to use the same light property type (which this is) with different lighting shaders
        // The class can do the necessary checks on the shader, throwing an error if it doesn't support them.
        // The application can set the texture uniforms on all the active shaders once only
        // No shaders are created by default, nothing is static
        // Downside: matching shaders and lights is up to the application programmer.
        //      Because shader compilation happens at run-time, it is difficult to do much better.
        std::reference_wrapper<const Shader> m_shader;
        std::reference_wrapper<const ShadowShader> m_shadowShader;
        View m_view;
        Vec4f m_intensity;

        GLuint m_shadowFrameBuffer;
        GLuint m_shadowDepthMap;
    };
}

#endif //AISLINN_LIGHT_H
