//
// Created by pieterjan on 24/07/2020.
//

#ifndef AISLINN_VIEW_H
#define AISLINN_VIEW_H

#include "glad.h"

#include "base.h"

namespace glAislinn
{

    /**
     * Basic Perspective View Class
     *
     * Encapsulates the GPU resource representing the view (+ perspective) transforms for use in the shader.
     * Can be used for defining cameras as well as lighting views (for shadow mapping etc.)
     *
     * Basic methods to change viewer position and orientation
     *
     * Assumes the z-direction points into the eye.
     * Returns the viewer transform that can be used to send to the uniform buffer block (so all shaders can use it).
     *
     * Pay close attention to how the various parameters are interpreted.
     *
     * Currently Aislinn does not have support for running WITHOUT a main camera instance (an instance from this class).
     */
    class View
    {
    public:
        View(float near, float far,
               float right, float top,
               Vec3f position = {0.f, 0.f, 0.f},
               Quatf orientation = Quatf(AngleAxisf(0.f, Vec3f{1, 0, 0})));

        // Core guidelines: C.21 state intent
        View(const View&) = delete;
        View operator=(const View&) = delete;
        // allow to move, so it can be used as a data member
        View(View&&) noexcept;
        View& operator=(View&&) noexcept;

        // explicit destructor, since this class wraps a GPU-side buffer
        ~View();

        // includes perspective transform
        Mat4f getViewTransform() const;

        Vec3f position() const;

        void setPosition(Vec3f position);
        void movePosition(const Vec3f& delta);

        // It is *crucial* to realise that the View class *uses* the rotation that says how to get the viewer in the right orientation
        // It is more natural for a human to think of the opposite, i.e. the rotation that we are applying to the Camera.
        // If you reason about the orientation in the latter way, make sure to invert before giving the rotation to the view.
        Quatf orientation() const;
        // Will not be inverted!
        void setOrientation(Quatf orientation);
        // Will not be inverted!
        void setOrientation(const AngleAxisf& orientation);

        // Look at works exactly the way you expect -- you don't have to worry about inverses here.
        void lookAt(const Vec3f& lookat = {0, 0, 0}, Vec3f up = {0, 1, 0});

        // send to gpu. Do this before drawing with the view.
        // or don't and ask yourself why there's nothing on the screen :-D
        // this is not ideal. Might be better to split up the "sync" and "use" action:
        //  We don't need to sync if the view is already on the gpu and it hasn't changed
        // For now, that does sound like premature optimization. Issue for another day.
        void sync(GLuint bindPointNumber = viewSettingsUniformBlockBindPoint) const;

    private:
        Mat4f getPerspectiveProjection() const;
        Mat4f getRotation() const;
        Mat4f getTranslation() const;

        float m_near, m_far, m_right, m_top;
        Vec3f m_position;
        Quatf m_orientation;

        GLuint m_viewSettingsBuffer;
    };
}

#endif //AISLINN_VIEW_H
